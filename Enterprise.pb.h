// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: Enterprise.proto

#ifndef GOOGLE_PROTOBUF_INCLUDED_Enterprise_2eproto
#define GOOGLE_PROTOBUF_INCLUDED_Enterprise_2eproto

#include <limits>
#include <string>

#include <google/protobuf/port_def.inc>
#if PROTOBUF_VERSION < 3018000
#error This file was generated by a newer version of protoc which is
#error incompatible with your Protocol Buffer headers. Please update
#error your headers.
#endif
#if 3018000 < PROTOBUF_MIN_PROTOC_VERSION
#error This file was generated by an older version of protoc which is
#error incompatible with your Protocol Buffer headers. Please
#error regenerate this file with a newer version of protoc.
#endif

#include <google/protobuf/port_undef.inc>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/arena.h>
#include <google/protobuf/arenastring.h>
#include <google/protobuf/generated_message_table_driven.h>
#include <google/protobuf/generated_message_util.h>
#include <google/protobuf/metadata_lite.h>
#include <google/protobuf/generated_message_reflection.h>
#include <google/protobuf/message.h>
#include <google/protobuf/repeated_field.h>  // IWYU pragma: export
#include <google/protobuf/extension_set.h>  // IWYU pragma: export
#include <google/protobuf/unknown_field_set.h>
// @@protoc_insertion_point(includes)
#include <google/protobuf/port_def.inc>
#define PROTOBUF_INTERNAL_EXPORT_Enterprise_2eproto
PROTOBUF_NAMESPACE_OPEN
namespace internal {
class AnyMetadata;
}  // namespace internal
PROTOBUF_NAMESPACE_CLOSE

// Internal implementation detail -- do not use these members.
struct TableStruct_Enterprise_2eproto {
  static const ::PROTOBUF_NAMESPACE_ID::internal::ParseTableField entries[]
    PROTOBUF_SECTION_VARIABLE(protodesc_cold);
  static const ::PROTOBUF_NAMESPACE_ID::internal::AuxiliaryParseTableField aux[]
    PROTOBUF_SECTION_VARIABLE(protodesc_cold);
  static const ::PROTOBUF_NAMESPACE_ID::internal::ParseTable schema[1]
    PROTOBUF_SECTION_VARIABLE(protodesc_cold);
  static const ::PROTOBUF_NAMESPACE_ID::internal::FieldMetadata field_metadata[];
  static const ::PROTOBUF_NAMESPACE_ID::internal::SerializationTable serialization_table[];
  static const ::PROTOBUF_NAMESPACE_ID::uint32 offsets[];
};
extern const ::PROTOBUF_NAMESPACE_ID::internal::DescriptorTable descriptor_table_Enterprise_2eproto;
class EnterpriseInfo;
struct EnterpriseInfoDefaultTypeInternal;
extern EnterpriseInfoDefaultTypeInternal _EnterpriseInfo_default_instance_;
PROTOBUF_NAMESPACE_OPEN
template<> ::EnterpriseInfo* Arena::CreateMaybeMessage<::EnterpriseInfo>(Arena*);
PROTOBUF_NAMESPACE_CLOSE

// ===================================================================

class EnterpriseInfo final :
    public ::PROTOBUF_NAMESPACE_ID::Message /* @@protoc_insertion_point(class_definition:EnterpriseInfo) */ {
 public:
  inline EnterpriseInfo() : EnterpriseInfo(nullptr) {}
  ~EnterpriseInfo() override;
  explicit constexpr EnterpriseInfo(::PROTOBUF_NAMESPACE_ID::internal::ConstantInitialized);

  EnterpriseInfo(const EnterpriseInfo& from);
  EnterpriseInfo(EnterpriseInfo&& from) noexcept
    : EnterpriseInfo() {
    *this = ::std::move(from);
  }

  inline EnterpriseInfo& operator=(const EnterpriseInfo& from) {
    CopyFrom(from);
    return *this;
  }
  inline EnterpriseInfo& operator=(EnterpriseInfo&& from) noexcept {
    if (this == &from) return *this;
    if (GetOwningArena() == from.GetOwningArena()
  #ifdef PROTOBUF_FORCE_COPY_IN_MOVE
        && GetOwningArena() != nullptr
  #endif  // !PROTOBUF_FORCE_COPY_IN_MOVE
    ) {
      InternalSwap(&from);
    } else {
      CopyFrom(from);
    }
    return *this;
  }

  inline const ::PROTOBUF_NAMESPACE_ID::UnknownFieldSet& unknown_fields() const {
    return _internal_metadata_.unknown_fields<::PROTOBUF_NAMESPACE_ID::UnknownFieldSet>(::PROTOBUF_NAMESPACE_ID::UnknownFieldSet::default_instance);
  }
  inline ::PROTOBUF_NAMESPACE_ID::UnknownFieldSet* mutable_unknown_fields() {
    return _internal_metadata_.mutable_unknown_fields<::PROTOBUF_NAMESPACE_ID::UnknownFieldSet>();
  }

  static const ::PROTOBUF_NAMESPACE_ID::Descriptor* descriptor() {
    return GetDescriptor();
  }
  static const ::PROTOBUF_NAMESPACE_ID::Descriptor* GetDescriptor() {
    return default_instance().GetMetadata().descriptor;
  }
  static const ::PROTOBUF_NAMESPACE_ID::Reflection* GetReflection() {
    return default_instance().GetMetadata().reflection;
  }
  static const EnterpriseInfo& default_instance() {
    return *internal_default_instance();
  }
  static inline const EnterpriseInfo* internal_default_instance() {
    return reinterpret_cast<const EnterpriseInfo*>(
               &_EnterpriseInfo_default_instance_);
  }
  static constexpr int kIndexInFileMessages =
    0;

  friend void swap(EnterpriseInfo& a, EnterpriseInfo& b) {
    a.Swap(&b);
  }
  inline void Swap(EnterpriseInfo* other) {
    if (other == this) return;
    if (GetOwningArena() == other->GetOwningArena()) {
      InternalSwap(other);
    } else {
      ::PROTOBUF_NAMESPACE_ID::internal::GenericSwap(this, other);
    }
  }
  void UnsafeArenaSwap(EnterpriseInfo* other) {
    if (other == this) return;
    GOOGLE_DCHECK(GetOwningArena() == other->GetOwningArena());
    InternalSwap(other);
  }

  // implements Message ----------------------------------------------

  inline EnterpriseInfo* New() const final {
    return new EnterpriseInfo();
  }

  EnterpriseInfo* New(::PROTOBUF_NAMESPACE_ID::Arena* arena) const final {
    return CreateMaybeMessage<EnterpriseInfo>(arena);
  }
  using ::PROTOBUF_NAMESPACE_ID::Message::CopyFrom;
  void CopyFrom(const EnterpriseInfo& from);
  using ::PROTOBUF_NAMESPACE_ID::Message::MergeFrom;
  void MergeFrom(const EnterpriseInfo& from);
  private:
  static void MergeImpl(::PROTOBUF_NAMESPACE_ID::Message* to, const ::PROTOBUF_NAMESPACE_ID::Message& from);
  public:
  PROTOBUF_ATTRIBUTE_REINITIALIZES void Clear() final;
  bool IsInitialized() const final;

  size_t ByteSizeLong() const final;
  const char* _InternalParse(const char* ptr, ::PROTOBUF_NAMESPACE_ID::internal::ParseContext* ctx) final;
  ::PROTOBUF_NAMESPACE_ID::uint8* _InternalSerialize(
      ::PROTOBUF_NAMESPACE_ID::uint8* target, ::PROTOBUF_NAMESPACE_ID::io::EpsCopyOutputStream* stream) const final;
  int GetCachedSize() const final { return _cached_size_.Get(); }

  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const final;
  void InternalSwap(EnterpriseInfo* other);
  friend class ::PROTOBUF_NAMESPACE_ID::internal::AnyMetadata;
  static ::PROTOBUF_NAMESPACE_ID::StringPiece FullMessageName() {
    return "EnterpriseInfo";
  }
  protected:
  explicit EnterpriseInfo(::PROTOBUF_NAMESPACE_ID::Arena* arena,
                       bool is_message_owned = false);
  private:
  static void ArenaDtor(void* object);
  inline void RegisterArenaDtor(::PROTOBUF_NAMESPACE_ID::Arena* arena);
  public:

  static const ClassData _class_data_;
  const ::PROTOBUF_NAMESPACE_ID::Message::ClassData*GetClassData() const final;

  ::PROTOBUF_NAMESPACE_ID::Metadata GetMetadata() const final;

  // nested types ----------------------------------------------------

  // accessors -------------------------------------------------------

  enum : int {
    kEntrpsNameFieldNumber = 1,
    kEntrpsAdrssFieldNumber = 3,
    kEntrpsSpesilzshnFieldNumber = 4,
    kYearOfCreatnFieldNumber = 2,
    kEntrpsForeignActvtyFieldNumber = 5,
  };
  // required string EntrpsName = 1;
  bool has_entrpsname() const;
  private:
  bool _internal_has_entrpsname() const;
  public:
  void clear_entrpsname();
  const std::string& entrpsname() const;
  template <typename ArgT0 = const std::string&, typename... ArgT>
  void set_entrpsname(ArgT0&& arg0, ArgT... args);
  std::string* mutable_entrpsname();
  PROTOBUF_MUST_USE_RESULT std::string* release_entrpsname();
  void set_allocated_entrpsname(std::string* entrpsname);
  private:
  const std::string& _internal_entrpsname() const;
  inline PROTOBUF_ALWAYS_INLINE void _internal_set_entrpsname(const std::string& value);
  std::string* _internal_mutable_entrpsname();
  public:

  // required string EntrpsAdrss = 3;
  bool has_entrpsadrss() const;
  private:
  bool _internal_has_entrpsadrss() const;
  public:
  void clear_entrpsadrss();
  const std::string& entrpsadrss() const;
  template <typename ArgT0 = const std::string&, typename... ArgT>
  void set_entrpsadrss(ArgT0&& arg0, ArgT... args);
  std::string* mutable_entrpsadrss();
  PROTOBUF_MUST_USE_RESULT std::string* release_entrpsadrss();
  void set_allocated_entrpsadrss(std::string* entrpsadrss);
  private:
  const std::string& _internal_entrpsadrss() const;
  inline PROTOBUF_ALWAYS_INLINE void _internal_set_entrpsadrss(const std::string& value);
  std::string* _internal_mutable_entrpsadrss();
  public:

  // optional string EntrpsSpesilzshn = 4;
  bool has_entrpsspesilzshn() const;
  private:
  bool _internal_has_entrpsspesilzshn() const;
  public:
  void clear_entrpsspesilzshn();
  const std::string& entrpsspesilzshn() const;
  template <typename ArgT0 = const std::string&, typename... ArgT>
  void set_entrpsspesilzshn(ArgT0&& arg0, ArgT... args);
  std::string* mutable_entrpsspesilzshn();
  PROTOBUF_MUST_USE_RESULT std::string* release_entrpsspesilzshn();
  void set_allocated_entrpsspesilzshn(std::string* entrpsspesilzshn);
  private:
  const std::string& _internal_entrpsspesilzshn() const;
  inline PROTOBUF_ALWAYS_INLINE void _internal_set_entrpsspesilzshn(const std::string& value);
  std::string* _internal_mutable_entrpsspesilzshn();
  public:

  // required int32 yearOfCreatn = 2;
  bool has_yearofcreatn() const;
  private:
  bool _internal_has_yearofcreatn() const;
  public:
  void clear_yearofcreatn();
  ::PROTOBUF_NAMESPACE_ID::int32 yearofcreatn() const;
  void set_yearofcreatn(::PROTOBUF_NAMESPACE_ID::int32 value);
  private:
  ::PROTOBUF_NAMESPACE_ID::int32 _internal_yearofcreatn() const;
  void _internal_set_yearofcreatn(::PROTOBUF_NAMESPACE_ID::int32 value);
  public:

  // optional bool EntrpsForeignActvty = 5;
  bool has_entrpsforeignactvty() const;
  private:
  bool _internal_has_entrpsforeignactvty() const;
  public:
  void clear_entrpsforeignactvty();
  bool entrpsforeignactvty() const;
  void set_entrpsforeignactvty(bool value);
  private:
  bool _internal_entrpsforeignactvty() const;
  void _internal_set_entrpsforeignactvty(bool value);
  public:

  // @@protoc_insertion_point(class_scope:EnterpriseInfo)
 private:
  class _Internal;

  // helper for ByteSizeLong()
  size_t RequiredFieldsByteSizeFallback() const;

  template <typename T> friend class ::PROTOBUF_NAMESPACE_ID::Arena::InternalHelper;
  typedef void InternalArenaConstructable_;
  typedef void DestructorSkippable_;
  ::PROTOBUF_NAMESPACE_ID::internal::HasBits<1> _has_bits_;
  mutable ::PROTOBUF_NAMESPACE_ID::internal::CachedSize _cached_size_;
  ::PROTOBUF_NAMESPACE_ID::internal::ArenaStringPtr entrpsname_;
  ::PROTOBUF_NAMESPACE_ID::internal::ArenaStringPtr entrpsadrss_;
  ::PROTOBUF_NAMESPACE_ID::internal::ArenaStringPtr entrpsspesilzshn_;
  ::PROTOBUF_NAMESPACE_ID::int32 yearofcreatn_;
  bool entrpsforeignactvty_;
  friend struct ::TableStruct_Enterprise_2eproto;
};
// ===================================================================


// ===================================================================

#ifdef __GNUC__
  #pragma GCC diagnostic push
  #pragma GCC diagnostic ignored "-Wstrict-aliasing"
#endif  // __GNUC__
// EnterpriseInfo

// required string EntrpsName = 1;
inline bool EnterpriseInfo::_internal_has_entrpsname() const {
  bool value = (_has_bits_[0] & 0x00000001u) != 0;
  return value;
}
inline bool EnterpriseInfo::has_entrpsname() const {
  return _internal_has_entrpsname();
}
inline void EnterpriseInfo::clear_entrpsname() {
  entrpsname_.ClearToEmpty();
  _has_bits_[0] &= ~0x00000001u;
}
inline const std::string& EnterpriseInfo::entrpsname() const {
  // @@protoc_insertion_point(field_get:EnterpriseInfo.EntrpsName)
  return _internal_entrpsname();
}
template <typename ArgT0, typename... ArgT>
inline PROTOBUF_ALWAYS_INLINE
void EnterpriseInfo::set_entrpsname(ArgT0&& arg0, ArgT... args) {
 _has_bits_[0] |= 0x00000001u;
 entrpsname_.Set(::PROTOBUF_NAMESPACE_ID::internal::ArenaStringPtr::EmptyDefault{}, static_cast<ArgT0 &&>(arg0), args..., GetArenaForAllocation());
  // @@protoc_insertion_point(field_set:EnterpriseInfo.EntrpsName)
}
inline std::string* EnterpriseInfo::mutable_entrpsname() {
  std::string* _s = _internal_mutable_entrpsname();
  // @@protoc_insertion_point(field_mutable:EnterpriseInfo.EntrpsName)
  return _s;
}
inline const std::string& EnterpriseInfo::_internal_entrpsname() const {
  return entrpsname_.Get();
}
inline void EnterpriseInfo::_internal_set_entrpsname(const std::string& value) {
  _has_bits_[0] |= 0x00000001u;
  entrpsname_.Set(::PROTOBUF_NAMESPACE_ID::internal::ArenaStringPtr::EmptyDefault{}, value, GetArenaForAllocation());
}
inline std::string* EnterpriseInfo::_internal_mutable_entrpsname() {
  _has_bits_[0] |= 0x00000001u;
  return entrpsname_.Mutable(::PROTOBUF_NAMESPACE_ID::internal::ArenaStringPtr::EmptyDefault{}, GetArenaForAllocation());
}
inline std::string* EnterpriseInfo::release_entrpsname() {
  // @@protoc_insertion_point(field_release:EnterpriseInfo.EntrpsName)
  if (!_internal_has_entrpsname()) {
    return nullptr;
  }
  _has_bits_[0] &= ~0x00000001u;
  return entrpsname_.ReleaseNonDefault(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(), GetArenaForAllocation());
}
inline void EnterpriseInfo::set_allocated_entrpsname(std::string* entrpsname) {
  if (entrpsname != nullptr) {
    _has_bits_[0] |= 0x00000001u;
  } else {
    _has_bits_[0] &= ~0x00000001u;
  }
  entrpsname_.SetAllocated(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(), entrpsname,
      GetArenaForAllocation());
  // @@protoc_insertion_point(field_set_allocated:EnterpriseInfo.EntrpsName)
}

// required int32 yearOfCreatn = 2;
inline bool EnterpriseInfo::_internal_has_yearofcreatn() const {
  bool value = (_has_bits_[0] & 0x00000008u) != 0;
  return value;
}
inline bool EnterpriseInfo::has_yearofcreatn() const {
  return _internal_has_yearofcreatn();
}
inline void EnterpriseInfo::clear_yearofcreatn() {
  yearofcreatn_ = 0;
  _has_bits_[0] &= ~0x00000008u;
}
inline ::PROTOBUF_NAMESPACE_ID::int32 EnterpriseInfo::_internal_yearofcreatn() const {
  return yearofcreatn_;
}
inline ::PROTOBUF_NAMESPACE_ID::int32 EnterpriseInfo::yearofcreatn() const {
  // @@protoc_insertion_point(field_get:EnterpriseInfo.yearOfCreatn)
  return _internal_yearofcreatn();
}
inline void EnterpriseInfo::_internal_set_yearofcreatn(::PROTOBUF_NAMESPACE_ID::int32 value) {
  _has_bits_[0] |= 0x00000008u;
  yearofcreatn_ = value;
}
inline void EnterpriseInfo::set_yearofcreatn(::PROTOBUF_NAMESPACE_ID::int32 value) {
  _internal_set_yearofcreatn(value);
  // @@protoc_insertion_point(field_set:EnterpriseInfo.yearOfCreatn)
}

// required string EntrpsAdrss = 3;
inline bool EnterpriseInfo::_internal_has_entrpsadrss() const {
  bool value = (_has_bits_[0] & 0x00000002u) != 0;
  return value;
}
inline bool EnterpriseInfo::has_entrpsadrss() const {
  return _internal_has_entrpsadrss();
}
inline void EnterpriseInfo::clear_entrpsadrss() {
  entrpsadrss_.ClearToEmpty();
  _has_bits_[0] &= ~0x00000002u;
}
inline const std::string& EnterpriseInfo::entrpsadrss() const {
  // @@protoc_insertion_point(field_get:EnterpriseInfo.EntrpsAdrss)
  return _internal_entrpsadrss();
}
template <typename ArgT0, typename... ArgT>
inline PROTOBUF_ALWAYS_INLINE
void EnterpriseInfo::set_entrpsadrss(ArgT0&& arg0, ArgT... args) {
 _has_bits_[0] |= 0x00000002u;
 entrpsadrss_.Set(::PROTOBUF_NAMESPACE_ID::internal::ArenaStringPtr::EmptyDefault{}, static_cast<ArgT0 &&>(arg0), args..., GetArenaForAllocation());
  // @@protoc_insertion_point(field_set:EnterpriseInfo.EntrpsAdrss)
}
inline std::string* EnterpriseInfo::mutable_entrpsadrss() {
  std::string* _s = _internal_mutable_entrpsadrss();
  // @@protoc_insertion_point(field_mutable:EnterpriseInfo.EntrpsAdrss)
  return _s;
}
inline const std::string& EnterpriseInfo::_internal_entrpsadrss() const {
  return entrpsadrss_.Get();
}
inline void EnterpriseInfo::_internal_set_entrpsadrss(const std::string& value) {
  _has_bits_[0] |= 0x00000002u;
  entrpsadrss_.Set(::PROTOBUF_NAMESPACE_ID::internal::ArenaStringPtr::EmptyDefault{}, value, GetArenaForAllocation());
}
inline std::string* EnterpriseInfo::_internal_mutable_entrpsadrss() {
  _has_bits_[0] |= 0x00000002u;
  return entrpsadrss_.Mutable(::PROTOBUF_NAMESPACE_ID::internal::ArenaStringPtr::EmptyDefault{}, GetArenaForAllocation());
}
inline std::string* EnterpriseInfo::release_entrpsadrss() {
  // @@protoc_insertion_point(field_release:EnterpriseInfo.EntrpsAdrss)
  if (!_internal_has_entrpsadrss()) {
    return nullptr;
  }
  _has_bits_[0] &= ~0x00000002u;
  return entrpsadrss_.ReleaseNonDefault(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(), GetArenaForAllocation());
}
inline void EnterpriseInfo::set_allocated_entrpsadrss(std::string* entrpsadrss) {
  if (entrpsadrss != nullptr) {
    _has_bits_[0] |= 0x00000002u;
  } else {
    _has_bits_[0] &= ~0x00000002u;
  }
  entrpsadrss_.SetAllocated(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(), entrpsadrss,
      GetArenaForAllocation());
  // @@protoc_insertion_point(field_set_allocated:EnterpriseInfo.EntrpsAdrss)
}

// optional string EntrpsSpesilzshn = 4;
inline bool EnterpriseInfo::_internal_has_entrpsspesilzshn() const {
  bool value = (_has_bits_[0] & 0x00000004u) != 0;
  return value;
}
inline bool EnterpriseInfo::has_entrpsspesilzshn() const {
  return _internal_has_entrpsspesilzshn();
}
inline void EnterpriseInfo::clear_entrpsspesilzshn() {
  entrpsspesilzshn_.ClearToEmpty();
  _has_bits_[0] &= ~0x00000004u;
}
inline const std::string& EnterpriseInfo::entrpsspesilzshn() const {
  // @@protoc_insertion_point(field_get:EnterpriseInfo.EntrpsSpesilzshn)
  return _internal_entrpsspesilzshn();
}
template <typename ArgT0, typename... ArgT>
inline PROTOBUF_ALWAYS_INLINE
void EnterpriseInfo::set_entrpsspesilzshn(ArgT0&& arg0, ArgT... args) {
 _has_bits_[0] |= 0x00000004u;
 entrpsspesilzshn_.Set(::PROTOBUF_NAMESPACE_ID::internal::ArenaStringPtr::EmptyDefault{}, static_cast<ArgT0 &&>(arg0), args..., GetArenaForAllocation());
  // @@protoc_insertion_point(field_set:EnterpriseInfo.EntrpsSpesilzshn)
}
inline std::string* EnterpriseInfo::mutable_entrpsspesilzshn() {
  std::string* _s = _internal_mutable_entrpsspesilzshn();
  // @@protoc_insertion_point(field_mutable:EnterpriseInfo.EntrpsSpesilzshn)
  return _s;
}
inline const std::string& EnterpriseInfo::_internal_entrpsspesilzshn() const {
  return entrpsspesilzshn_.Get();
}
inline void EnterpriseInfo::_internal_set_entrpsspesilzshn(const std::string& value) {
  _has_bits_[0] |= 0x00000004u;
  entrpsspesilzshn_.Set(::PROTOBUF_NAMESPACE_ID::internal::ArenaStringPtr::EmptyDefault{}, value, GetArenaForAllocation());
}
inline std::string* EnterpriseInfo::_internal_mutable_entrpsspesilzshn() {
  _has_bits_[0] |= 0x00000004u;
  return entrpsspesilzshn_.Mutable(::PROTOBUF_NAMESPACE_ID::internal::ArenaStringPtr::EmptyDefault{}, GetArenaForAllocation());
}
inline std::string* EnterpriseInfo::release_entrpsspesilzshn() {
  // @@protoc_insertion_point(field_release:EnterpriseInfo.EntrpsSpesilzshn)
  if (!_internal_has_entrpsspesilzshn()) {
    return nullptr;
  }
  _has_bits_[0] &= ~0x00000004u;
  return entrpsspesilzshn_.ReleaseNonDefault(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(), GetArenaForAllocation());
}
inline void EnterpriseInfo::set_allocated_entrpsspesilzshn(std::string* entrpsspesilzshn) {
  if (entrpsspesilzshn != nullptr) {
    _has_bits_[0] |= 0x00000004u;
  } else {
    _has_bits_[0] &= ~0x00000004u;
  }
  entrpsspesilzshn_.SetAllocated(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(), entrpsspesilzshn,
      GetArenaForAllocation());
  // @@protoc_insertion_point(field_set_allocated:EnterpriseInfo.EntrpsSpesilzshn)
}

// optional bool EntrpsForeignActvty = 5;
inline bool EnterpriseInfo::_internal_has_entrpsforeignactvty() const {
  bool value = (_has_bits_[0] & 0x00000010u) != 0;
  return value;
}
inline bool EnterpriseInfo::has_entrpsforeignactvty() const {
  return _internal_has_entrpsforeignactvty();
}
inline void EnterpriseInfo::clear_entrpsforeignactvty() {
  entrpsforeignactvty_ = false;
  _has_bits_[0] &= ~0x00000010u;
}
inline bool EnterpriseInfo::_internal_entrpsforeignactvty() const {
  return entrpsforeignactvty_;
}
inline bool EnterpriseInfo::entrpsforeignactvty() const {
  // @@protoc_insertion_point(field_get:EnterpriseInfo.EntrpsForeignActvty)
  return _internal_entrpsforeignactvty();
}
inline void EnterpriseInfo::_internal_set_entrpsforeignactvty(bool value) {
  _has_bits_[0] |= 0x00000010u;
  entrpsforeignactvty_ = value;
}
inline void EnterpriseInfo::set_entrpsforeignactvty(bool value) {
  _internal_set_entrpsforeignactvty(value);
  // @@protoc_insertion_point(field_set:EnterpriseInfo.EntrpsForeignActvty)
}

#ifdef __GNUC__
  #pragma GCC diagnostic pop
#endif  // __GNUC__

// @@protoc_insertion_point(namespace_scope)


// @@protoc_insertion_point(global_scope)

#include <google/protobuf/port_undef.inc>
#endif  // GOOGLE_PROTOBUF_INCLUDED_GOOGLE_PROTOBUF_INCLUDED_Enterprise_2eproto
